﻿using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace System.Windows.Input
{
	/// <summary>
	/// ICommand を参照するオブジェクト
	/// </summary>
	/// <remarks>Freezable を継承することで FrameworkElement でなくとも子要素に DataContext を継承できる</remarks>
	public class CommandReference : Freezable, ICommand
	{
		#region コンストラクタ

		/// <summary>
		/// CommandReferenceを初期化します。
		/// </summary>
		public CommandReference()
		{
		}

		#endregion

		#region プロパティ

		/// <summary>
		/// ICommand オブジェクトを取得または設定します。
		/// </summary>
		public ICommand Command
		{
			get { return (ICommand)GetValue(CommandProperty); }
			set { SetValue(CommandProperty, value); }
		}
		/// <summary>
		/// ICommand オブジェクトを取得または設定します。
		/// </summary>
		public static readonly DependencyProperty CommandProperty =
			DependencyProperty.Register("Command", typeof(ICommand), typeof(CommandReference), new FrameworkPropertyMetadata(null, 
				(d, e) =>
				{
					var commandReference = d as CommandReference;
					if (commandReference != null)
					{
						var oldCommand = e.OldValue as ICommand;
						if (oldCommand != null)
						{
							oldCommand.CanExecuteChanged -= commandReference.CanExecuteChanged;
						}
						var newCommand = e.NewValue as ICommand;
						if (newCommand != null)
						{
							newCommand.CanExecuteChanged += commandReference.CanExecuteChanged;
						}
					}
				}));

		/// <summary>
		/// コマンドパラメータを取得または設定します。
		/// </summary>
		public object CommandParameter
		{
			get { return (object)GetValue(CommandParameterProperty); }
			set { SetValue(CommandParameterProperty, value); }
		}
		/// <summary>
		/// コマンドパラメータを取得または設定します。
		/// </summary>
		public static readonly DependencyProperty CommandParameterProperty =
			DependencyProperty.Register("CommandParameter", typeof(object), typeof(CommandReference), new PropertyMetadata(null));

		#endregion

		#region イベント

		/// <summary>
		/// コマンドを実行するかどうかに影響するような変更があった場合に発生します。
		/// </summary>
		public event EventHandler CanExecuteChanged;

		#endregion

		#region メソッド

		/// <summary>
		/// 現在の状態でコマンドが実行可能かどうかを決定するメソッドを定義します。
		/// </summary>
		/// <param name="parameter">コマンドにより使用されるデータです。 コマンドにデータを渡す必要がない場合は、このオブジェクトを null に設定できます。</param>
		/// <returns></returns>
		public bool CanExecute(object parameter)
		{
			parameter = this.CommandParameter;
			return this.Command?.CanExecute(parameter) ?? false;
		}

		/// <summary>
		/// コマンドが起動される際に呼び出すメソッドを定義します。
		/// </summary>
		/// <param name="parameter">コマンドにより使用されるデータです。 コマンドにデータを渡す必要がない場合は、このオブジェクトを null に設定できます。</param>
		public void Execute(object parameter)
		{
			parameter = this.CommandParameter;
			this.Command?.Execute(parameter);
		}

		protected override Freezable CreateInstanceCore()
		{
			return new CommandReference();
		}

		#endregion
	}
}
