﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CommandGroupTest
{
	public class MainViewModel : ViewModelBase
	{
		#region コンストラクタ

		/// <summary>
		/// MainViewModelを初期化します。
		/// </summary>
		public MainViewModel()
		{
			this.Test1Command = new RelayCommand<string>(ExecuteTest1, CanExecuteTest1);
			this.Test2Command = new RelayCommand(ExecuteTest2, CanExecuteTest2);
		}

		#endregion

		#region プロパティ

		private bool m_Test1Mode = true;
		/// <summary>
		/// Test1モードを取得または設定します。
		/// </summary>
		public bool Test1Mode
		{
			get { return m_Test1Mode; }
			set
			{
				if (m_Test1Mode == value)
					return;
				m_Test1Mode = value;
				RaisePropertyChanged();

				this.Test1Command.RaiseCanExecuteChanged();
				this.Test2Command.RaiseCanExecuteChanged();
			}
		}




		#endregion

		#region コマンド

		public RelayCommand<string> Test1Command { get; private set; }
		/// <summary>
		/// Test1コマンド
		/// </summary>
		private void ExecuteTest1(string parameter)
		{
			MessageBox.Show("Test1: " + parameter ?? String.Empty);
		}
		private bool CanExecuteTest1(string parameter)
		{
			return Test1Mode;
		}

		public RelayCommand Test2Command { get; private set; }
		/// <summary>
		/// Test2コマンド
		/// </summary>
		private void ExecuteTest2()
		{
			MessageBox.Show("Test2");
		}
		private bool CanExecuteTest2()
		{
			return !Test1Mode;
		}



		#endregion

	}
}
