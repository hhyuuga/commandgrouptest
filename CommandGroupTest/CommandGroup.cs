﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace System.Windows.Input
{
	/// <summary>
	/// 複数の ICommand をグループ化します。
	/// </summary>
	/// <remarks>FreezableCollection を継承することで FrameworkElement でなくとも子要素に DataContext を継承できる</remarks>
	public class CommandGroup : FreezableCollection<CommandReference>, ICommand, INotifyCollectionChanged
	{
		#region コンストラクタ

		/// <summary>
		/// CommandGroupを初期化します。
		/// </summary>
		public CommandGroup()
		{
			this.CollectionChanged += OnCollectionChanged;
		}

		#endregion

		#region プロパティ

		/// <summary>
		/// コレクションの変更時ハンドラ
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCollectionChanged(object sender, Collections.Specialized.NotifyCollectionChangedEventArgs e)
		{
			if (e.OldItems?.Count > 0)
			{
				foreach (ICommand cmd in e.OldItems)
				{
					cmd.CanExecuteChanged -= OnChildCommandCanExecuteChanged;
				}
			}
			if (e.NewItems?.Count > 0)
			{
				foreach (ICommand cmd in e.NewItems)
				{
					cmd.CanExecuteChanged += OnChildCommandCanExecuteChanged;
				}
			}
		}

		/// <summary>
		/// CommandGroup の各コマンドの CanExecuteChanged イベントのハンドラ
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnChildCommandCanExecuteChanged(object sender, EventArgs e)
		{
			this.CanExecuteChanged?.Invoke(sender, e);
		}

		#endregion

		#region イベント

		/// <summary>
		/// コマンドを実行するかどうかに影響するような変更があった場合に発生します。
		/// </summary>
		public event EventHandler CanExecuteChanged;
		/// <summary>
		/// コレクションが変更されたときに発生します。
		/// </summary>
#pragma warning disable 0067
		public event NotifyCollectionChangedEventHandler CollectionChanged;
#pragma warning restore 0067

		#endregion

		#region メソッド

		/// <summary>
		/// 現在の状態でコマンドが実行可能かどうかを決定するメソッドを定義します。
		/// <para>コレクション内に CanExecute() が true を返すコマンドがあれば true, それ以外は false を返します。</para>
		/// </summary>
		/// <param name="parameter">コマンドにより使用されるデータです。 コマンドにデータを渡す必要がない場合は、このオブジェクトを null に設定できます。</param>
		/// <returns></returns>
		public bool CanExecute(object parameter)
		{
			foreach (var cmd in this)
			{
				if (cmd.CanExecute(cmd.CommandParameter))
				{
					return true;
				}
			}
			return false;
		}

		/// <summary>
		/// コマンドが起動される際に呼び出すメソッドを定義します。
		/// <para>コレクション内の有効なコマンドをすべて実行します。</para>
		/// </summary>
		/// <param name="parameter">コマンドにより使用されるデータです。 コマンドにデータを渡す必要がない場合は、このオブジェクトを null に設定できます。</param>
		public void Execute(object parameter)
		{
			foreach (var cmd in this)
			{
				cmd.Execute(cmd.CommandParameter);
			}
		}

		#endregion
	}
}
